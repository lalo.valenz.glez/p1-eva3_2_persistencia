//
//  ViewController.swift
//  Eva3_2_Persistencia
//
//  Created by TEMPORAL2 on 24/11/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtText: UITextField!
    
    @IBOutlet weak var lblMostrar: UILabel!
    
    @IBAction func guardar(sender: AnyObject) {
        let pathTemp = NSTemporaryDirectory()
        let urlTemp = NSURL(fileURLWithPath: pathTemp)
        let miArchivo = urlTemp.URLByAppendingPathComponent("mitexto.txt")
        do{
            try txtText.text?.writeToURL(miArchivo, atomically: true, encoding: NSUTF8StringEncoding)//String.Encoding
        } catch _ {
            lblMostrar.text = "Algo Salio Mal ):"
        }
    }
    
    
    @IBAction func leer(sender: AnyObject) {
        let pathTemp = NSTemporaryDirectory()
        let urlTemp = NSURL(fileURLWithPath: pathTemp)
        let fileTemp = urlTemp.URLByAppendingPathComponent("mitexto.txt")
        do{
            txtText.text = try String(contentsOfFile: fileTemp.path!)
        } catch _ {
            lblMostrar.text = "Algo Salio Mal ):"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

